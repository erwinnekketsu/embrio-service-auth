package main

import (
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/transport/grpc"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	mysql.InitCon()

	grpc.Run()
}
