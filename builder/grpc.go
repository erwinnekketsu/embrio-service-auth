package builder

import (
	"encoding/json"

	"gitlab.com/erwinnekketsu/embrio-service-auth.git/entity"
	pb "gitlab.com/erwinnekketsu/embrio-service-auth.git/transport/grpc/proto/auth"
)

type Grpc struct{}

func NewGrpc() *Grpc {
	return &Grpc{}
}

// GetLoginrResponse generate response
func (g *Grpc) GetLoginrResponse(userToken entity.UserToken) (res *pb.LoginResponse) {
	bytes, _ := json.Marshal(&userToken)
	_ = json.Unmarshal(bytes, &res)

	return
}

// GetDetailUserResponse generate response
func (g *Grpc) GetDetailUserResponse(data entity.UserData) (res *pb.CheckTokenResponse) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

func (g *Grpc) LoginRequest(reqPb *pb.LoginRequest) entity.LoginRequest {
	return entity.LoginRequest{
		Username: reqPb.Username,
		Password: reqPb.Password,
		Token:    reqPb.Token,
	}
}

func (g *Grpc) CheckTokenRequest(reqPb *pb.CheckTokenRequest) entity.UserToken {
	return entity.UserToken{
		Token: reqPb.Token,
	}
}
