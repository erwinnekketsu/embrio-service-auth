package grpc

import (
	"context"

	action "gitlab.com/erwinnekketsu/embrio-service-auth.git/action"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/builder"
	pb "gitlab.com/erwinnekketsu/embrio-service-auth.git/transport/grpc/proto/auth"
)

type GrpcServer struct{ builder *builder.Grpc }

func (gs *GrpcServer) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	var resp pb.LoginResponse
	token, err := action.NewLogin().Handler(ctx, req)
	resp.Token = token
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) CheckToken(ctx context.Context, req *pb.CheckTokenRequest) (*pb.CheckTokenResponse, error) {
	var resp pb.CheckTokenResponse
	data, err := action.NewCheckToken().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return gs.builder.GetDetailUserResponse(data), nil
}

func (gs *GrpcServer) LoginAdmin(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	var resp pb.LoginResponse
	data, err := action.NewLoginAdmin().Handler(ctx, req)
	resp.Token = data.Token
	resp.Name = data.FullName
	resp.Role = int32(data.Role)
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) CheckTokenAdmin(ctx context.Context, req *pb.CheckTokenRequest) (*pb.CheckTokenResponse, error) {
	var resp pb.CheckTokenResponse
	data, err := action.NewCheckTokenAdmin().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return gs.builder.GetDetailUserResponse(data), nil
}

func NewGrpcServer() *GrpcServer {
	return &GrpcServer{
		builder: builder.NewGrpc(),
	}
}
