package util

import (
	"encoding/base64"
	"log"
	"strconv"
	"time"

	"golang.org/x/crypto/bcrypt"
)

const (
	DateFormatRFC3339 = time.RFC3339
)

var StatusCodeSuccess = []string{"200", "201", "202"}
var StatusClientSlice = []string{"capture", "settlement", "expire", "pending"}

func GetRedisKey(domain, id string) string {
	return domain + ":" + id
}

func FormatDateToRFC3339(t time.Time) string {
	return t.Format(DateFormatRFC3339)
}

func StringToInt(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}

func StringToInt64(s string) int64 {
	i, _ := strconv.ParseInt(s, 10, 64)
	return i
}

func EncodeBase64(s string) string {
	return base64.StdEncoding.EncodeToString([]byte(s))
}

func BcryptPassword(s string) string {
	passoword := []byte(s)

	hashedPassword, err := bcrypt.GenerateFromPassword(passoword, bcrypt.DefaultCost)
	if err != nil {
		return ""
	}
	return string(hashedPassword)
}

func GenerateToken(email string) string {
	emailBytes := []byte(email)

	hash, err := bcrypt.GenerateFromPassword(emailBytes, bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		return ""
	}

	return base64.StdEncoding.EncodeToString(hash)
}
