package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-auth.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-auth.git/transport/grpc/proto/auth"
)

type CheckToken struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewCheckToken() *CheckToken {
	return &CheckToken{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (l *CheckToken) Handler(ctx context.Context, req *pb.CheckTokenRequest) (userData entity.UserData, err error) {
	checkTokenRequest := l.builder.CheckTokenRequest(req)
	data, err := l.repoUser.CheckToken(ctx, checkTokenRequest)
	if err != nil {
		return userData, err
	}
	userData.ID = data.ID
	userData.Email = data.Email
	userData.Status = data.Status
	return userData, nil
}
