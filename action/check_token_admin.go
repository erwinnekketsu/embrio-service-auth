package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-auth.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-auth.git/transport/grpc/proto/auth"
)

type CheckTokenAdmin struct {
	builder   *builder.Grpc
	repoAdmin repo.IAdmin
}

func NewCheckTokenAdmin() *CheckTokenAdmin {
	return &CheckTokenAdmin{
		builder:   builder.NewGrpc(),
		repoAdmin: repo.NewAdmin(),
	}
}

func (l *CheckTokenAdmin) Handler(ctx context.Context, req *pb.CheckTokenRequest) (userData entity.UserData, err error) {
	checkTokenRequest := l.builder.CheckTokenRequest(req)
	data, err := l.repoAdmin.CheckTokenAdmin(ctx, checkTokenRequest)
	if err != nil {
		return userData, err
	}
	userData.ID = data.ID
	userData.Email = data.Email
	return userData, nil
}
