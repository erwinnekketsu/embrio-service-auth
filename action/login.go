package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-auth.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-auth.git/transport/grpc/proto/auth"
)

type Login struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewLogin() *Login {
	return &Login{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (l *Login) Handler(ctx context.Context, req *pb.LoginRequest) (token string, err error) {
	loginRequest := l.builder.LoginRequest(req)
	data, err := l.repoUser.Login(ctx, loginRequest)
	if err != nil {
		return "nil", err
	}
	token = data.Token
	return token, nil
}
