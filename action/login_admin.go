package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-auth.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-auth.git/transport/grpc/proto/auth"
)

type LoginAdmin struct {
	builder   *builder.Grpc
	repoAdmin repo.IAdmin
}

func NewLoginAdmin() *LoginAdmin {
	return &LoginAdmin{
		builder:   builder.NewGrpc(),
		repoAdmin: repo.NewAdmin(),
	}
}

func (la *LoginAdmin) Handler(ctx context.Context, req *pb.LoginRequest) (dataAdmin entity.AdminData, err error) {
	loginRequest := la.builder.LoginRequest(req)
	data, err := la.repoAdmin.LoginAdmin(ctx, loginRequest)
	if err != nil {
		return dataAdmin, err
	}
	dataAdmin.Token = data.Token
	dataAdmin.FullName = data.FullName
	dataAdmin.Role = data.Role
	return dataAdmin, nil
}
