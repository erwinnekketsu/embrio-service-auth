package entity

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Token    string `json:"token"`
}

type UserToken struct {
	Token string `json:"token"`
}

type UserData struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Token    string `json:"token"`
	Status   int    `json:"status"`
}

type AdminData struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	FullName string `json:"full_name"`
	Password string `json:"password"`
	Token    string `json:"token"`
	IsActive int    `json:"is_active"`
	Role     int    `json:"role"`
}
