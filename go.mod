module gitlab.com/erwinnekketsu/embrio-service-auth.git

go 1.15

require (
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.3.0
	go.elastic.co/apm/module/apmgrpc v1.12.0
	go.elastic.co/apm/module/apmsql v1.12.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	google.golang.org/grpc v1.39.0
	google.golang.org/grpc/examples v0.0.0-20210715165331-ce7bdf50abb1 // indirect
	google.golang.org/protobuf v1.27.1
)
