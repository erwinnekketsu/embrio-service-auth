package repo

import (
	"context"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/util"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/util/errors"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	iMysql   mysql.IMysql
	embrioDB *sqlx.DB
}

func NewUser() *User {
	return &User{
		iMysql:   mysql.NewClient(),
		embrioDB: mysql.EmbrioDB,
	}
}

func (u *User) Login(ctx context.Context, req entity.LoginRequest) (data mysql.UserData, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"username$eq!": req.Username,
		},
	}

	// get data by username
	err = u.iMysql.Get(ctx, u.embrioDB, &data, query, mysql.QueryFindUserByEmail)
	if err != nil {
		return data, errors.ErrBadRequest("Username Not exist")
	}

	password := data.Password
	err = bcrypt.CompareHashAndPassword([]byte(password), []byte(req.Password))
	if err != nil {
		return data, errors.ErrBadRequest("Password not match")
	}

	queryStatus := &util.Query{
		Filter: map[string]interface{}{
			"username$eq!": req.Username,
			"status$eq!":   1,
		},
	}

	// get data by username
	err = u.iMysql.Get(ctx, u.embrioDB, &data, queryStatus, mysql.QueryFindUserByEmail)
	if err != nil {
		return data, errors.ErrBadRequest("Accoun not verify, please contact admin.")
	}

	if req.Token != "" {
		queryToken := &util.Query{
			Filter: map[string]interface{}{
				"token$eq!": req.Token,
			},
		}
		dataDevice := mysql.Devices{}
		err = u.iMysql.Get(ctx, u.embrioDB, &dataDevice, queryToken, mysql.QueryGetDevices)
		if err != nil {
			log.Println(err)
		}

		var queryDevice string
		if dataDevice.UserId == 0 {
			queryDevice = mysql.QueryCreateDevices
		} else {
			if dataDevice.UserId == data.ID {
				return data, nil
			} else {
				queryDevice = mysql.QueryUpdateDevices
			}
		}
		userId := data.ID
		dateNow := time.Now().UTC()
		dataDevice = mysql.Devices{
			UserId:    userId,
			Token:     req.Token,
			CreatedAt: &dateNow,
			UpdatedAt: &dateNow,
		}
		_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, dataDevice, queryDevice)
		if err != nil {
			log.Println("create Device")
			log.Println(err)
		}
	}

	return data, nil
}

func (u *User) CheckToken(ctx context.Context, req entity.UserToken) (data mysql.UserData, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"token$eq!": req.Token,
		},
	}

	// get data by username
	err = u.iMysql.Get(ctx, u.embrioDB, &data, query, mysql.QueryCheckToken)
	if err != nil {
		return data, errors.ErrBadRequest("Token Not valid")
	}

	return data, nil
}
