package mysql

const (
	QueryFindUserByEmail = `
		SELECT id, username, email, password, status, token FROM nasabah
	`

	QueryCheckToken = `
		SELECT id, email, status FROM nasabah
	`

	QueryGetAdminInfo = `
		SELECT id, usernameAdmin, emailAdmin, namaLengkapAdmin, password, is_active, token, role FROM admin
	`

	QueryCreateDevices = `
		INSERT INTO devices (idNasabah, token, createdAt) VALUES(:idNasabah, :token, :createdAt)
	`

	QueryUpdateDevices = `
		UPDATE devices SET idNasabah=:idNasabah, updatedAt=:updatedAt WHERE token=:token
	`

	QueryDeleteDevices = `
		DELETE FROM devices WHERE idNasabah=:idNasabah
	`

	QueryGetDevices = `
		SELECT idNasabah, token FROM devices
	`
)
