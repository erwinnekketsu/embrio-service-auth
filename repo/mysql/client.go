package mysql

import (
	"context"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/util"
)

type Client struct{}

func NewClient() *Client {
	return &Client{}
}

func (c *Client) Get(ctx context.Context, db *sqlx.DB, data interface{}, query *util.Query, queryString string) (err error) {
	where, args := query.Where()
	q := queryString
	q += where

	if err = db.GetContext(ctx, data, db.Rebind(q), args...); err != nil {
		return
	}

	return
}

func (c *Client) CreateOrUpdate(ctx context.Context, db *sqlx.DB, data interface{}, query string) (lastId int64, err error) {
	log.Println("CreateOrUpdate request: ", query)
	res, err := db.NamedExecContext(ctx, query, data)
	if err != nil {
		log.Println(err)
		return 0, err
	}
	lastId, _ = res.LastInsertId()
	log.Println("CreateOrUpdate response: ", lastId)
	return lastId, err
}
