package mysql

import "time"

type UserData struct {
	ID       int    `json:"id" db:"id"`
	Username string `json:"username" db:"username"`
	Email    string `json:"email" db:"email"`
	Password string `json:"password" db:"password"`
	Token    string `json:"token" db:"token"`
	Status   int    `json:"status" db:"status"`
}

type AdminData struct {
	ID       int    `json:"id" db:"id"`
	Username string `json:"username" db:"usernameAdmin"`
	Email    string `json:"email" db:"emailAdmin"`
	FullName string `json:"full_name" db:"namaLengkapAdmin"`
	Password string `json:"password" db:"password"`
	Token    string `json:"token" db:"token"`
	IsActive int    `json:"is_active" db:"is_active"`
	Role     int    `json:"role" db:"role"`
}

type Devices struct {
	ID        int        `json:"id" db:"id"`
	UserId    int        `json:"user_id" db:"idNasabah"`
	Token     string     `json:"token" db:"token"`
	CreatedAt *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt" db:"updatedAt"`
}
