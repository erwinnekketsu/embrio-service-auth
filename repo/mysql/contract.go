package mysql

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/util"
)

type IMysql interface {
	Get(context.Context, *sqlx.DB, interface{}, *util.Query, string) error
	CreateOrUpdate(ctx context.Context, db *sqlx.DB, data interface{}, query string) (lastId int64, err error)
}
