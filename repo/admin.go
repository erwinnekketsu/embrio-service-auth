package repo

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/util"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/util/errors"
	"golang.org/x/crypto/bcrypt"
)

type Admin struct {
	iMysql   mysql.IMysql
	embrioDB *sqlx.DB
}

func NewAdmin() *Admin {
	return &Admin{
		iMysql:   mysql.NewClient(),
		embrioDB: mysql.EmbrioDB,
	}
}

func (a *Admin) LoginAdmin(ctx context.Context, req entity.LoginRequest) (data mysql.AdminData, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"usernameAdmin$eq!": req.Username,
		},
	}

	// get data by username
	err = a.iMysql.Get(ctx, a.embrioDB, &data, query, mysql.QueryGetAdminInfo)
	if err != nil {
		return data, errors.ErrBadRequest("Username Not exist")
	}

	password := data.Password
	err = bcrypt.CompareHashAndPassword([]byte(password), []byte(req.Password))
	if err != nil {
		return data, errors.ErrBadRequest("Password not match")
	}

	return data, nil
}

func (a *Admin) CheckTokenAdmin(ctx context.Context, req entity.UserToken) (data mysql.AdminData, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"token$eq!": req.Token,
		},
	}

	err = a.iMysql.Get(ctx, a.embrioDB, &data, query, mysql.QueryGetAdminInfo)
	if err != nil {
		return data, errors.ErrBadRequest("Token Not valid")
	}

	return data, nil
}
