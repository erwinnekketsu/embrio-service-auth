package repo

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-auth.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-auth.git/repo/mysql"
)

type IUser interface {
	Login(ctx context.Context, req entity.LoginRequest) (data mysql.UserData, err error)
	CheckToken(ctx context.Context, req entity.UserToken) (data mysql.UserData, err error)
}

type IAdmin interface {
	LoginAdmin(ctx context.Context, req entity.LoginRequest) (data mysql.AdminData, err error)
	CheckTokenAdmin(ctx context.Context, req entity.UserToken) (data mysql.AdminData, err error)
}
